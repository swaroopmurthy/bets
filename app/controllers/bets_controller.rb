# frozen_string_literal: true

class BetsController < ApplicationController
  def show
    @bets = if params[:id]
              Bet.by_date(params[:id])
            else
              []
        end
  end
end
