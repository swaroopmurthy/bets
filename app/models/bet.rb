# frozen_string_literal: true

require 'csv'

class Bet
  def self.by_date(date)
    csv_rows = CSV.read('db/data.csv', headers: true)
    csv_rows.select { |row| date.to_date == row['start_time'].to_date }
  end
end
